\chapter{Background Research} \label{ch:backres}

The initial research question (can be found in section \ref{subsec:inires}) conjectures two different research areas. First of all how public places can be transformed, to reach a better and higher quality. Second, how media technology can be applied in the process. To uncover the mentioned areas, different methods, theories and similar projects are researched.

\section{Transforming Public Places}

\subsection{Qualities of a public space}

Professor Jan Gehl and Dr. Litt are stating \cite{professor_gehl_public_????} in their related work, Public Spaces for a Changing Public Life, that 

\begin{quote}
“The life or lifelessness of public spaces depends very much on the quality of the spaces and whether they are welcoming to likely users, to walk, stay, sit or otherwise enjoy the spaces.”
\end{quote}

Meaning, that adding quality to an unappealing area could change the life of the public space, by inviting people to it through quality. This might sound trite at the first reading, but public spaces have gone through a lot of change in the modern society and the modern individual’s perception of a public space. Previously, public spaces were visited out of necessity and served as a meeting area for people. Nowadays, less people are using public spaces because they have to, but more because they want to \cite{professor_gehl_public_????}.

However, the existing tendencies show that quality drag the attention of people.

\begin{quotation}
“The universal finding is that whenever quality is provided people come. Access to other people, to possibilities for experience and recreation among others are in high demand, and these opportunities have important roles for upholding or strengthening the overall policies for friendly, humane, open, democratic and safe societies.”
\end{quotation}

Professor Jan Gehl and Dr. Litt are categorizing qualities into three major categories: 

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{gehl_litt.png}
\caption{The three major categories and key qualities \cite{professor_gehl_public_????}.}
\label{fig:gehl_litt}
\end{figure}

Each category contains numerous key qualities, which are giving a brief explanation how each category is characterized.

\begin{enumerate}
\item{Protection/Safety} 
Safety can be provided through designing or transforming the space to provide protection from traffic, crime and unpleasant sense experiences, such as pollution.

\item{Comfort} 
Comfort can be added through possibilities for key activities in the area (fx. instead of one general activity, the area provides more kinds of) or extending those. Key activities could vary from one place to another but in general those are possibilities for fx. walking, talking, listening, “seeing” and to unfold different activities.

\item{Enjoyment} could be provided through “scaling” the surrounding buildings and possibilities for traffic to meet the audience’s expectations. It can also be provided through adding possibilities to enjoy positive aspects of the climate and aesthetics/positive sense-experiences. 
\end{enumerate}

The goal of this project is not to provide a solution to all of these key qualities, but to find a tool and provide an activity or activities through it, while adding to the quality of the public space in all three major categories.

  

\subsection{Lighting}

To provide safety, comfort and enjoyment, the chosen tools for creating an activity or installation, is lighting. Night-time presents challenges to cities globally,  for reasons of safety and fear or lack of attraction \cite{lam_cities_2015}. Lighting does not only uncover areas that are obscured by darkness, but by providing the feeling of safety, it would attract people to extend their daytime activities into night-time, while also providing possibilities for new ones. According to David P. Farringtons’ and Brandon C. Welshs’  study \cite{welsh_effects_2008},  improvements in street lighting offer a cost-effective crime reduction measure and should be considered as an important element in situational crime reduction programmes. Although, Denmark is the third least infracted country in Europe, there is always a place for improvements. The fact that there is a close relation between light and our sense of safety, does also support the utilization of urban lighting. According to the study of Stephen Atkins et al \cite{atkins_influence_1991}, lighting provides reassurance to some people who are fearful in their use of public space, particularly women. 

Lighting up the nighttime of a public space can provide the quality of comfort to visitors. Professor Jan Gehl and Dr. Litt characterize the comfort of a given public space, as a quality that is built up by providing possibilities to key activities such as to walk, stay and see. Although lighting, as a purely functional element, can not provide new activities to the area, it can extend existing key activities from the daytime. Lighting can also support social qualities and activities of an urban area. A study, made by Arup Lighting, lists several lighting installations which are created to support the vibrant night-life in a truly 24-hours city on the western hemisphere of the globe  \cite{lam_cities_2015}. According to the study, the importance of supporting the social qualities and activities - which can be characterized through different timelines of the night (can be seen on figure \ref{fig:shades}) - with urban city lighting, lies within the increasing economy of the night-life of a city. “In 2009, the UK’s night-time economy was estimated to generate 27\% of total urban turnover, while Sydney was able to create \$2.7bn of economic benefits with only \$127m spending on nighttime management.” \cite{lam_cities_2015} When designing for night-time, the complexity of its economic and social uses have to be considered all the time, and design lighting as the enabler of urban activities. Lighting can support the economy and social uses of an area by providing positive sense experiences and creating an inviting experience, in order to also make it enjoyable.

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{shades.png}
\caption{The eight different shades of night and their generic activities respectively \cite{lam_cities_2015}.}
\label{fig:shades}
\end{figure}
\pagebreak
\section{The State of the Art} \label{sec:sota}

\subsection{Arup Lighting}

Arup is an independent collective of designers, planners, engineers, consultants and technical specialists offering a broad range of professional services. The organization consists of different groups, with different work areas in focus. Arup Lighting involves groups, that are creating strategies to design and implement solutions of innovative lighting on urban places. Their strategies involve understanding the way how people are using existing urban places and implement their overall vision in the context of the needs of the users.

\subsubsection{Bruum Ruum, Barcelona}

Bruum Ruum (figure \ref{fig:bruum}) by artec3 Studio and David Torrents  \cite{_descubre_2014}, is an interactive lighting installation, in Barcelona, that is creating a dialogue between people and the public space. The installation invites the visitors to control the lighting of the area by their own sound. Without interacting visitors, the installation uses the surrounding sounds of traffic. This “passive” interaction acts as an invitation towards the people in the area. The installation acts as a stand-alone system; it is able to operate without control from another system. Bruum Ruum is a social light and sound interaction, where the public space takes on a human quality - creativity - by reacting to sounds of the city, with changing its colours accordingly.

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{bruum.png}
\caption{Bruum Ruum, Barcelona (Retrieved from https://goo.gl/HnpSkj). }
\label{fig:bruum}
\end{figure}
\pagebreak

\subsubsection{Marling, Eindhoven}

Marling by Umbrellium (figure \ref{fig:marling}), is an interactive lighting installation, in Eindhoven, that is involving the sound of people to create new dimensions into a public place, which appears plain during the daytime. With the utilization of a series of lasers, smoke and microphones, the system is able respond to the voice of people by creating various colorful waves of light as an answer. Marling is not only a strategy to create a new understanding of the public space compared to the daytime, but to involve social interaction between the visitors and invite them to change the shape of the space with group work. “In Marling, people become players on the urban stage, together bringing to life a large outdoor public space through their actions and sounds, and building a – sometimes indescribable – shared public memory of collaboration that lasts long after the event.” \cite{_citizen_????}

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{marling.png}
\caption{Marling, Eindhoven \cite{_citizen_????}. }
\label{fig:marling}
\end{figure}
\pagebreak

\subsection{Esben Bala Skouboe’s Work}

Esben Bala Skouboe is an awarded PhD. civil engineer in Architecture \& Design from Aalborg University, Denmark, currently an external lecturer at Lighting Design. His work revolves around urban architecture and responsive lighting design. In 2012, part of his PhD. work, he designed and tested two similar systems at Kennedy Station, Aalborg \cite{poulsen_controlling_2012} and Gammel torv, Aalborg. The first system’s base condition was tested using wind-, mobile phone- and movement adaptive, responsive lighting. To adapt the illumination of the square to the movement of people, the system used movement tracking based on the processed live footage of a thermal camera.  In general, people did not notice the change in lighting while walking through the public space, but people watching from the edge of the area noticed the interaction between the illumination and people. The test also concluded on that the use of the intelligent LED lighting system reduced the power consumption of the area by 90\%. In the second system, Esben Bala Skouboe tested three different activities (treasure hunt \cite{_red_????}, light aura \cite{_white_????} and smoulder \cite{_glowing_????}) to the base condition of the lighting, with similar results to the test of the first system.

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{esben.png}
\caption{The responsive light system at Kennedy Station \cite{_white_????}. }
\label{fig:esben}
\end{figure}
