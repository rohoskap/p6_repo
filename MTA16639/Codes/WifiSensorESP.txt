
#include <ESP8266WiFi.h>
#include <WiFiUDP.h>

extern "C" {  //required for read Vdd Voltage
#include "user_interface.h"
  // uint16 readvdd33(void);
}

int status = WL_IDLE_STATUS;
const char* sensornr = "# 1 ";
const char* requestip = "RequestIP1";
const char* requestdata = "RequestData1";
const char* requeststop = "RequestStop1";
const char* ssid = "MELTINGSPACE";  //  your network SSID (name)
const char* pass = "1010101010";       // your network password
byte inByte[] = {0,0,0};
IPAddress ipair ;

unsigned int localPort = 12345;      // local port to listen for UDP packets

byte packetBuffer[512]; //buffer to hold incoming and outgoing packets
bool senddata = 0;
bool sendbyte = 0;

// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;

void printWifiStatus() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi shield's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);
} 

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(250000);
  while (!Serial) 
	{
		; // wait for serial port to connect. Needed for Leonardo only
	}

  // setting up Station AP
  WiFi.begin(ssid, pass);
  
  // Wait for connect to AP
  Serial.print("[Connecting]");
  Serial.print(ssid);
  int tries=0;
  while (WiFi.status() != WL_CONNECTED) 
	{
		delay(500);
		Serial.print(".");
		tries++;
		if (tries > 30)
			{
				break;
			}
	}
  Serial.println();


  printWifiStatus();

  Serial.println("Connected to wifi");
  Serial.print("Udp server started at port ");
  Serial.println(localPort);
  Udp.begin(localPort);
  pinMode(2, OUTPUT);
}

void loop()
{
	
	if (senddata) 
	{
	if (Serial.available() == 3 || Serial.available() == 6) 
		{
			Serial.println(Serial.available());
			Serial.readBytes(inByte,3);
			sendbyte =1;
		}
	if (Serial.available() != 3 && !sendbyte )
		{
		for (int i = 0; i<Serial.available(); i++)
			{
				Serial.println(Serial.available());
				Serial.read();
			}
		}

}
  
  
  int noBytes = Udp.parsePacket();
  String received_command = "";
  
  if ( noBytes ) {
    Serial.print(millis() / 1000);
    Serial.print(":Packet of ");
    Serial.print(noBytes);
    Serial.print(" received from ");
    Serial.print(Udp.remoteIP());
    Serial.print(":");
    Serial.println(Udp.remotePort());
    // We've received a packet, read the data from it
    Udp.read(packetBuffer,noBytes); // read the packet into the buffer

    // display the packet contents in HEX
    for (int i=1;i<=noBytes;i++)
    {
		Serial.print(packetBuffer[i-1],HEX);
		received_command = received_command + char(packetBuffer[i - 1]);
		if (i % 32 == 0)
			{
				Serial.println();
			}
		else Serial.print(' ');
    }
   
    Serial.println(received_command);  

      
    if (received_command == requestip)   // <----------------------------- IP Request Case
		{
			Serial.println(" Client has sent IP Request.... ");
			Udp.beginPacket(Udp.remoteIP(), 54321);
			Udp.print(sensornr);
			Udp.println(WiFi.localIP());
			Udp.endPacket();
		}

    if (received_command == requestdata)  // <----------------------------- IP Request Data - Forward serial input to udp
		{
			Serial.println(" Client has Requested Serial Data.... ");
			Udp.beginPacket(Udp.remoteIP(), 5432);
			senddata = 1;
			ipair = Udp.remoteIP();
			Udp.print(sensornr);
			Udp.write("Enabled");
			Udp.endPacket();
		}

    if (received_command == requeststop)  // <----------------------------- IP Request Data - Forward serial input to udp
		{
			Serial.println(" Client has Requested to stop Serial Data.... ");
			Udp.beginPacket(Udp.remoteIP(), 5432);
			senddata = 0;
			Udp.print(sensornr);
			Udp.write("Disabled");
			Udp.endPacket();
		}
	Serial.println();
  } 


    if (sendbyte)
		{
			Udp.beginPacket(ipair, 54325);
			Udp.print(sensornr);
			Udp.write(inByte[0]);
			Udp.write(inByte[1]);
			Udp.write(inByte[2]);
			Udp.endPacket();
			sendbyte=0;
			//Serial.println("Sent");
		}
	delay(2);
	analogWrite(2, HIGH);  //this section sends request for arduino interupt to execute code sending 3 bytes with sensor values
	delayMicroseconds(900);
	analogWrite(2, LOW);
}

