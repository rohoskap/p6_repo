\chapter{Evaluation}

\section{Method}

In order to properly evaluate the designed and implemented artefact, similar projects have been reviewed. The work of Poulsen \cite{poulsen_controlling_2012}, utilizes observations, interviews and video logging and Pihlajaniemi et al. \cite{pihlajaniemi_experiencing_2012} in addition to data logging and interviews, asks the users to fill in a questionnaire.

Due to the constraints of the project, a combined strategy was used; merging
observations, video logging, interviews and questionnaires. The questionnaires consisted of selected Intrinsic Motivation Inventory scales (IMI - a tool to measure subjective user experience scales) and several Flow State Scale categories (FSS – measuring Flow) categories \cite{jackson_development_1996} \cite{morrison_intrinsic_2016}. 
The data obtained would then be used to evaluate the designed activity and user's attitude towards the interactive lamp system.  
In addition to IMI, FSS and demographic questions, qualitative
questions were asked, covering the general feelings evoked to the user which they were asked to name,
the effect of such an installation on the area or recommendation to friends. This data would be used to see what effect the installation has on an area in regards to its quality. The following list summarizes the questions used:

\begin{enumerate}
\item Intrinsic Motivation Inventory (IMI) scales:
\begin{itemize}
\item Interest/enjoyment
\item Effort/importance
\item Pressure/tension
\item Value/Usefulness
\item  Relatedness
\end{itemize}

\item Flow State Scale (FSS) categories:
\begin{itemize}
\item Action-awareness merging
\item Unambiguous feedback
\item Concentration on task at hand
\item  Sense of control
\item Loss of self-consciousness
\item Transformation of time
\item Autotelic experience
\end{itemize}

\item Qualitative measures
\begin{itemize}
\item Describe what looking at the installation makes you feel in five words
\item The installation improved the quality of the area.
\item Providing such an activity/installation makes the area more interesting.
\item I would recommend this activity to my friends.
\item I would re-visit this area purely for the purpose of experiencing the installation
\end{itemize}
\end{enumerate}

The given response to the question was measured on 7-point likert scales, where 1 stood for “strongly disagree” and 7 for “strongly agree”. Some of the questions had to be reversed in order to comply with the rest. See the full list of the questions in the appendix. For the analysis, the following general hypothesis and null/alternative hypotheses have been formed: 

\begin{quotation}
H: Such an installation improves the quality of an area and provides a positive experience.
\end{quotation}

\begin{quotation}
H1 0: The installation does not affect the quality of the area, or affects it negatively (qScore is less than or equal to 4).
\end{quotation}

\begin{quotation}
H1 A: The installation has a positive effect on the quality of the area (qScore is greater than 4).
\end{quotation}

\begin{quotation}
H2 0: Users have a neutral or negative feeling towards the experience/activity (qScore is less than or equal 4).
\end{quotation}

\begin{quotation}
H2 A: Users have a better than neutral feeling towards the experience/activity (qScore is greater than 4).
\end{quotation}

Two test rounds were carried out, on the 12th and 16th of May with changes in-between the tests to both the artefact and the method and procedure. The tests were carried out at Platform 4 in Karolinelund, Aalborg, Denmark and before data collection, all of the participants read and signed the informed consent.

\section{First Test}

Both the first and the second rounds share similarities described above in this chapter. The differences are described further.

\subsection{Method, setup and procedure}

The test method was designed to allow the users to observe the interaction with the artefact and fill in a questionnaire covering three IMI scales (interest/enjoyment, value/usefulness and relatedness) and three qualitative questions regarding area appeal and activity description. Following, they would interact with the system in groups as described in activity one (can be found in section \ref{sec:activityones}, requesting colour change verbally and fill in the remaining part of the questionnaire afterwards. The users were observed during their interaction and interviewed in-between. 

The test was set up with two stages, one where the users interacted with the installation and one where other users observed the interaction and filled in the questionnaires, see figure \ref{fig:setup1}.

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{Setup1.png}
\caption{First test setup, where white circles represent lamps, green dots represent sensor placement, the black rectangle represents the observation/questionnaire area and red dots represent camera placement.}
\label{fig:setup1}
\end{figure}

Due to lack of resources, handling all the participants at once proved challenging and some interview data may have been lost in the process. In addition to that, having all the people interact with the artefact in groups and one group after another resulted in queues on the questionnaires. This essentially diminished the user’s immediate feelings after taking part in the experience, possibly affecting the questionnaire data.

For the first test round, 157 participants were invited, out of which around 20 attended, some invited by other participants and one random passer-by. Altogether, 19 people filled the first questionnaire and 17 filled both. The two participants did not wish to experience the installation. 

Out of the participants, 73.7 \% were male, 26.3 \% were female, aged from 20 to 36 years, with the average experience with lighting installations rated to 4.42. The background showed that out of 19, 10 participants had a visual-related background.
\subsection{Analysis}
The plan for the analysis was to compare the two questionnaires’ responses against each other. However, due to the procedure, most participants filled both of the questionnaires at once. Therefore, a single-sample Wilcoxon-Signed-Rank test has been used, on all the gathered data, for each question in a subset. In order to reject the null hypotheses, the scores had to indicate a better experience and flow than average (4 on a 7-scale Likert-scale). 
\subsubsection{IMI, Flow and qualitative questions results}
Interest/enjoyment, pressure/tension, value/usefulness and relatedness scales all rejected the null hypotheses in all questions of the scales. However, 5 out of 9 flow questions, one qualitative question and all questions under effort/importance failed to do so. The spreadsheet containing all the individual p-values can be seen in appendix \ref{app:aD}. These are:

\begin{enumerate}
\item Qualitative:
\begin{itemize}
\item I would re-visit this area purely for the purpose of experiencing the activity again.
\end{itemize}
\item Effort/importance:
\begin{itemize}
\item It was important for me to perform well on this task.
\item I put a lot of effort into the task.
\item I tried very hard to do well at this activity.
\end{itemize}
\item Flow:
\begin{itemize}
\item I had a strong sense of what I wanted to do. 
\item I loved the feeling of that performance and want to capture it again.
\item I was not concerned with how I was presenting myself.
\item I was not worried about what others may have been thinking of me.
\end{itemize}
\end{enumerate}

The two following questions are a specific case. The first failed to reject the null hypothesis with a rather low p-value (0.067), yet the second question did reject it. Such ratings can be appropriated to the fact that some participants only performed the task for around two minutes.

\begin{itemize}
\item Time seemed to alter (either slowed down or speeded up).
\item It felt like time went by quickly.
\end{itemize}

When asked to describe the activity using five words, from both the distance and after experiencing the installation, the most frequently used adjectives were positive. “Interesting” scored highest in both instances, scoring 8 (in combination with “curious”) and 5, respectively. Most adjectives in the first instance related to “energetic” (7), with happiness, fun and confusion scoring equally (6). In the second instance, a shift can be observed. In addition to “interesting” scoring less, the activity is described as more physical (9), fun (9), but also “not too serious” and related (7).

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{tableDESC.png}
\caption{A table summarizing the word description on the activity from both the distance and the experience point of view. Note that some of the related descriptions have been pooled together.}
\label{fig:tableDESC}
\end{figure}

The highest scoring colour choosing preference was in the form of buttons on the lamp (52.9 \%).

\subsubsection{Discussion of the results}
Video analysis also shows behaviour linked to the questions failing to reject the null hypothesis, as well as the descriptions of the activity. “I had a strong sense of what I wanted to do” may be linked to confusion. In the beginning of the test, some participants tried to figure out how the system works and discussing it: 

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{1discussion.png}
\caption{A group of three participants discussing the system.}
\label{fig:discussion}
\end{figure}

Other groups may have been confused due to delayed feedback from the lamps, after their movement did not trigger an immediate response:

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{Confusion.png}
\caption{A participant is turning around with delayed feedback.}
\label{fig:confusion}
\end{figure}

“I loved the feeling of the performance and want to capture it again” can potentially be linked to more passive individuals in the groups (see figure \ref{fig:IndividualEffort}), for which the activity was not as interesting. Another reason for such result is the activity being described as “simple” or “not too serious” by some users, seeing it as a momentary entertainment.




\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{IndividualEffort.png}
\caption{A passive member of a group seen standing on the right.}
\label{fig:IndividualEffort}
\end{figure}

The negative answers to questions “I was not concerned with how I was presenting myself.” and “I was not worried about what others may have been thinking of me.” can be related to the fact that the participants had to slow their movement or even move away from the sensors in order to request a colour change, resulting in breaks without any activity:

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{Break.png}
\caption{A group of participants decides to change the colour and take a break.}
\label{fig:Break}
\end{figure}

Moreover, observing participants trying to figure the activity may have thought that in order to perform well, they were required to replicate other participants’ movements. These may have appeared silly and awkward, possibly forcing them to restrain their movement.

Low scores across the effort/importance scale may be related to the fact that based on the descriptions used, some considered it an activity, while others approached it as an art installation. As both of these activities require different amounts of effort, different people rated it according to their approach. 

Some participants described the experience as “laggy” and “slow”. Related behaviour has also been observed, for example when a participant approached the lamps closer, because he expected them to react faster the closer he moves:

\begin{figure}[H]
\includegraphics[width=0.9\textwidth]{LampReaction.png}
\caption{A participant expecting the lamp to sense his activity.}
\label{fig:LampReaction}
\end{figure}

This can be related to the expectation of the sensor placement being on the lamps, rather on hotspot closeby. Such implementation would change the way the feedback is provided to the user, mirroring their movement instead of filling the array with a colour.